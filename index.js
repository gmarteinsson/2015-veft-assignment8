'use strict';

const express = require('express');
const api = require('./api');	// search for api.js in this folder

const mongoose = require('mongoose');
const port = 4000;
const app = express();

app.use('/api', api);	// binds route request to api.js

//Connect to MongoDB
// Name of the db cat with 'localhost/
// Make sure to let app.listen after connection to mongodb is ON.
mongoose.connect('localhost/app');
mongoose.connection.once('open', function() {
	console.log('mongoose is connected');
	app.listen(port, function () {
		console.log('server starting on port:', port);
	});
});


