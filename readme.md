#How to run the application

We assume you already have mongodb installed on your computer. If not you can download it [here](https://www.mongodb.org/downloads#production).

 
**1.** clone the project ```git clone git@bitbucket.org:gmarteinsson/2015-veft-assignment8.git```

**2.** go into the folder ```cd 2015-veft-assignment8```

**3.** make a directory called data

**4.** run ```npm install```

**5.** run ```npm install --save-dev nodemon```

**6.** in a different terminal run ```mongod --dbpath data``` to run the database

**7.** in yet another terminal run ```npm run watch``` to run the server

**8.** curl away!

![alt text](http://1.bp.blogspot.com/-j0wWmHElOzY/UfK9vpz0l3I/AAAAAAAAVwk/nTkoNsoKLJs/s1600/Arnold+Lifting+Cats.gif "curl that cat!")

![alt text](http://i.imgur.com/b0swr6V.gif "I pitty the fool that curled wrong!")

Instructions on how to use mongodb can be found in the document mongodb_instructions.pdf in the source. The instructions were written by Hlynur Sigurthorsson.

#Instructions

##Authentication

Some of the endpoints that we implement need to be authenticated. The authentication method that we use is a simple token based authentication. For example in the GET /api/user route (described below), clients need to add a header named TOKEN to the request with a token value known only by the user. This token is stored within the user document in MongoDB. On the server-side we identify the user by this token.

There should also be a ADMIN_TOKEN for an administrators. The value of that token can be hard-coded in the code. Endpoints, such as post /api/company, need to be authenticated using this token.

##MongoDB

In this assignment we should have a single database named app with three collections, users, companies and punchcards.

##Users

In the app.users collection we store documents which describe users. User document should have the following fields.

_id: Unique id for the user.

name: String which represents the name of the user.

token: The token value for this user. Used by the user-client to endpoints that need authorisation and authentication of users. This can be a randomly generated value (The uuid node module can be handy here).

age: Integer value representing the age of the user

gender: String with a single character m, f or o. These character stand for male, female or other respectively.

##Companies

In the app.companies collection we store documents representing companies that have been added to the system and can give out punch cards. Company document should have the following fields.

_id: Unique id for the company.

name: String for the company name.

description: Company description.

punchcard_lifetime: Number of days which a punch card given out by this company should live.

##Punchcards

In the app.punchcards we store documents which represents active punch-cards. A given punch card should have the following fields.

_id: representing the punch card id.

company_id: company id.

user_id: user id.

created: time stamp when this punch card was created.

##Routes

The following routes should be implemented in this assignment.

**GET /api/company**

Fetches a list of companies that have been added to MongoDB. This endpoint should not use any authentication. If no company has been added this endpoint should return an empty list.

**GET /api/company/:id**

Fetches a given company that has been added to MongoDB by id. This endpoints should return a single JSON document if found. Example output.

If no company is found by the id then this endpoint should return response with status code 404. No authentication is needed for this endpoint.

**POST /api/company**

Allows administrators to add new companies to MongoDB. The company is posted with a POST method and the data sent as a JSON object within the request body. This endpoint should be authenticated using the ADMIN_TOKEN header.

The following requirements should hold for this endpoint.

If admin token is missing or is incorrect (the ADMIN_TOKEN value can be hard-coded in the server) the server should respond with status code 401.
If the payload is not valid (fields missing or have invalid types) the service should not add the document to MongoDB and respond with 412 (Precondition failed).
If company was successful this endpoint answers with status code 201 and returns a JSON document with the company id, {company_id: ...}.

**GET /user**

Returns a list of all users that are in the MongoDB. This endpoint is not authenticated and the token value within the user document must be removed from the document before it is written to the response.

**POST /punchcard/:company_id**

Creates a new punch card for company :company_id. This endpoint is authenticated using the user token. This endpoint should work as follows.

Clients sends a request with TOKEN value in the header. That value is used to authenticate the user. A new document is created in the app.punchcards collections with the user_id which owns the TOKEN value found in the header.

The following requirements should hold for this endpoint:

If no user is found by the TOKEN, or the TOKEN value is missing then the server should respond with status code 401.
If no company is found by the company_id within the url, the server should respond with 404.
If the user already has a working punch card (based on the created and punchcard_lifetime) the server should answer with 409.
If you decide to add some more data to a punchcard, and that data is invalid in the request, the server responds with status code 412.
Otherwise the server should return with a status code 201 and return the newly created id back.