'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const _ = require('lodash');
const models = require('./models');
const api = express();
const ADMIN_TOKEN = 'admino';
/*
	Returns list of all users.
*/
api.get('/user', (req, res) => {
  models.User.find({}, function (err, docs) {
    if (err) {
      res.status(500).send(err);
    } else {

      var returnValue = [];

      for (var i = 0; i < docs.length; i++) {
        var returnUser = {
          _id: docs[i]._id,
          name: docs[i].name,
          age: docs[i].age,
          gender: docs[i].gender
        };
        returnValue.push(returnUser);
      }
      res.send(returnValue);
    }
  });
});

/*
	Posts a new user to the database.
*/
api.post('/user', bodyParser.json(), (req, res) => {
  const m = new models.User(req.body);

  const err = m.validateSync();

  if (err) {
    res.status(412).send(err.message);
    return;
  }

  m.save(function(err,doc) {
    if(err) {
      res.status(500).send(err);
    } else {
      res.send(doc);
      return;
    }
  });
});

/*
	Returns a list of companies.
*/
api.get('/company', (req, res) => {
  models.Company.find({}, function(err, docs) {
    if (err) {
      res.status(500).send(err);
    } else {
      res.send(docs);
    }
  });
});


/*
	Returns a company object from the id that was passed in.
	Returns an error if no company with his id.
*/
api.get('/company/:id', (req, res) => {
  const id = req.params.id;


  models.Company.findOne({'_id': id}, function(err, docs) {
  	
    if (err) {
      res.status(404).send(err);
    } else {
    	if(!docs) {
    		res.status(404).send("Company not found");
    	}
    	else {
      		res.send(docs);
    	}
    }
  });
});

/*
	Posts a company to the database.
	Returns an error if models.Company isn't validted.
*/
api.post('/company', bodyParser.json(), (req, res) => {
  const m = new models.Company(req.body);
  const admin_token = req.headers.admin_token;

	// 1. if the admin token is not equal to
	// ADMIN_TOKEN the server should response with 401.
  if (!admin_token || admin_token !== ADMIN_TOKEN) {
    res.status(401).send('UNAUTHORIZED');
    return;
  }

  // 2. if the payload is not valid the service should not add the
  // document to MONGODB and respons with 412 precondition failed
  const payLoadError = m.validateSync();

  if (payLoadError) {
    res.status(412).send(payLoadError.message);
    return;
  }

	// 3. If company was successful this endpoint answers with status
	//	code 201 and returns a JSON document with the company id....

  m.save(function(err,doc) {
    if (err) {
      res.status(500).send(err);
      return;
    }
    else {
      res.status(201).send(doc);
      return;
    }
  });
});

api.post('/punchcard/:company_id', bodyParser.json(), (req, res) => {
  const id = req.headers.token;
  const company = req.params.company_id;

  /*
  Creates a new punch card for company :company_id. This endpoint is authenticated using the user token. This endpoint should work as follows.

  Clients sends a request with TOKEN value in the header. That value is used to authenticate the user. A new document is created in the app.punchcards collections with the user_id which owns the TOKEN value found in the header.

  The following requirements should hold for this endpoint:

  If you decide to add some more data to a punchcard, and that data is invalid in the request, the server responds with status code 412.
  Otherwise the server should return with a status code 201 and return the newly created id back.
  */


  models.User.findOne({'token': id}, (err, data) => {

    // user Token should be in header. This value is to authenticate user.
    // if no user is found by the TOKEN, or TOKEN value is missing the server should return 401
    if (err) {
      res.status(500).send(err);
      return;
    }

    if (!data) {
      res.status(401).send(err);
      return;
    }

    console.log('we found a user in database');
    console.log('user data: ', data);


    // If no company is found by the company_id within the url, the server should respond with 404.
    models.Company.findOne({_id : company}, (err, data) => {
      if (err) {
        res.status(500).send(err);
        return;
      }

      if (!data) {
        res.status(404).send(err);
        return;
      }
      console.log('company and user where found');
      console.log('company data: ', data);

      // milliseconds per day: 1000*60*60*24 = 86400000
      var millisecondsInADay = 86400000;
      var expDate = _.now() - data.punchcard_lifetime * millisecondsInADay;

      //If the user already has a working punch card (based on the created and punchcard_lifetime) the server should answer with 409.
      models.Punchcard.findOne({'company_id': company, 'user_id': id, 'created': { $gt: expDate} }, (err, data) => {
        if (err) {
          res.status(500).send(err.message);
          return;
        }
        // if we find a working punchcard
        if (data) {
          res.status(409).send(err);
          return;
        }

        const m = new models.Punchcard({
          company_id: company,
          user_id: id,
          created: _.now()
        });

        m.save((err,doc) => {
          if (err) {
            res.status(412).send(err);
          } else {
            res.status(201).send({'_id': doc._id});
            return;
          }
        });
      });
    });
  });
});

module.exports = api;
