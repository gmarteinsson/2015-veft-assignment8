'use strict'

const mongoose = require('mongoose');
const uuid = require('node-uuid');

const UserSchema = mongoose.Schema({

	//String which represents the name of the user.
	name: {
		type: String,
		required: true,
		maxlength: 100,
		minlength: 1
	},	
	//The token value for this user. Used by the user-client to endpoints that need authorisation and authentication of users. This can be a randomly generated value (The uuid node module can be handy here).
	//token: Number,
	//Integer value representing the age of the user
	age: {
		type: Number,
		min: 0,
		max: 150
	},	
	//String with a single character m, f or o. These character stand for male, female or other respectively.
	gender: {
		type: String,
		maxlength: 1
	},
	token: {
		type: String,
		default: uuid
	}

});

const CompanySchema = mongoose.Schema({
	name: {
		type: String,
		required: true,
	},
	description: {
		type: String,
		maxlength: 200,
	},
	punchcard_lifetime: {
		type: Number,
		min: 1,
	}
});

const PunchcardSchema = mongoose.Schema({
	company_id: {
		type: String,
		required: true
	},
	user_id: {
		type: String
	},
	created: {
		type: Date,
		default: new Date()
	}
});

module.exports = {
	User:		mongoose.model('User', UserSchema),
	Company:	mongoose.model('Company', CompanySchema),
	Punchcard:	mongoose.model('Punchcard', PunchcardSchema)
};